import Vue from 'vue'
import Router from 'vue-router'
import MainPage from '@/components/MainPage'
import CategoryPage from '@/components/CategoryPage'
import OrderPage from '@/components/OrderPage'
import ProductPage from '@/components/ProductPage'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'MainPage',
      component: MainPage
    },
    {
      path: '/order',
      name: 'OrderPage',
      component: OrderPage
    },
    {
      path: '/category/:name',
      name: 'CategoryPage',
      component: CategoryPage
    },
    {
      path: '/products/:id',
      name: 'ProductPage',
      component: ProductPage
    }
  ]
})
