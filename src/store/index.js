import Vue from 'vue'
import Vuex from 'vuex'
import api from '../api'
var qs = require('qs')

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    sections: [],
    currentSection: {},
    currentCategory: '',
    products: [],
    product: {},
    ordered: [],
    orderData: {}
  },
  getters: {
    sections (state) {
      return state.sections || []
    },
    categories: (state) => {
      let section = state.currentSection || {}
      return section.categories || []
    },
    products (state) {
      return state.products || []
    },
    product (state) {
      return state.product || {}
    },
    ordered (state) {
      return state.ordered || []
    },
    currentCategory (state, getters) {
      return getters.categories.recursiveFind(state.currentCategory) || {}
    },
    orderData (state) {
      return state.orderData || {}
    }
  },
  mutations: {
    setSections (state, payload) {
      Vue.set(state, 'sections', payload.data.results)
      Vue.set(state, 'currentSection', payload.data.results[0])
    },
    setCategories (state, payload) {
      Vue.set(state, 'categories', payload.data.results)
    },
    setProducts (state, payload) {
      Vue.set(state, 'products', payload.data.results)
    },
    setCurrentCategory (state, payload) {
      Vue.set(state, 'currentCategory', payload)
    },
    setOrdered (state, payload) {
      Vue.set(state, 'ordered', payload)
    },
    setProduct (state, payload) {
      Vue.set(state, 'product', payload.data)
    },
    setOrderData (state, payload) {
      Vue.set(state, 'orderData', payload)
      Vue.localStorage.set('order_number', payload.number)
      Vue.localStorage.set('_oounjwt', payload.token)
    },
    restoreOrderData (state) {
      let number = Vue.localStorage.get('order_number')
      let token = Vue.localStorage.get('_oounjwt')
      if (number && token) {
        Vue.set(state, 'orderData', {token: token, number: number})
      }
    }
  },
  actions: {
    initApp ({ commit, dispatch }) {
      dispatch('getAllSections')
      dispatch('getAllProducts')
      commit('restoreOrderData')
      api.get('/orders/' + this.getters.orderData.number + '/', {
        params: {
          token: this.getters.orderData.token
        }
      }).then((response) => {
        commit('setOrdered', response.data.payload)
      })
    },
    getAllSections ({ commit }) {
      api.get('/sections/').then((response) => commit('setSections', response))
    },
    getAllProducts ({ commit }) {
      api.get('/products/').then((response) => commit('setProducts', response))
    },
    addToCart ({commit}, { product }) {
      let tmp = this.getters.ordered.slice()
      if (tmp.length === 0) {
        tmp.push(product)
        api.post('/orders/', tmp).then((response) => {
          commit('setOrdered', tmp)
          commit('setOrderData', response.data)
        })
      } else {
        tmp.push(product)
        api.patch('/orders/' + this.getters.orderData.number + '/', {token: this.getters.orderData.token, payload: tmp.map((order) => order.id)}).then((response) => {
          commit('setOrdered', tmp)
          commit('setOrderData', response.data)
        })
      }
    },
    filterProducts ({ commit }, data) {
      api.get('/products/', {
        params: data,
        paramsSerializer: function (params) {
          return qs.stringify(params, {arrayFormat: 'repeat'})
        }
      }).then((response) => {
        commit('setProducts', response)
        commit('setCurrentCategory', data.category)
      })
    },
    getProduct ({ commit }, { id }) {
      api.get('/products/' + id).then((response) => {
        commit('setProduct', response)
        commit('setCurrentCategory', response.data.category)
      })
    },
    clearCurrentProduct ({ commit }) {
      commit('setProduct', {})
    },
    removeItemFromOrder ({ commit }, { item }) {
      let newOrder = this.getters.ordered.slice()
      let index = newOrder.findIndex((o) => o.id === item.id)
      newOrder.splice(index, 1)
      if (newOrder.length === 0) {
        api.delete('/orders/' + this.getters.orderData.number + '/',
          {
            params: {
              token: this.getters.orderData.token
            }
          }).then((response) => {
          commit('setOrdered', newOrder)
          commit('setOrderData', {})
        })
      } else {
        api.patch('/orders/' + this.getters.orderData.number + '/', {token: this.getters.orderData.token, payload: newOrder.map((order) => order.id)}).then((response) => {
          commit('setOrdered', newOrder)
          commit('setOrderData', response.data)
        })
      }
      commit('setOrdered', newOrder)
    },
    addItemToOrder ({ commit }, { item }) {
      let newOrder = this.getters.ordered.slice()
      newOrder.push(item)
      if (newOrder.length === 0) {
        api.delete('/orders/' + this.getters.orderData.number + '/',
          {
            params: {
              token: this.getters.orderData.token
            }
          }).then((response) => {
          commit('setOrdered', newOrder)
          commit('setOrderData', {})
        })
      } else {
        api.patch('/orders/' + this.getters.orderData.number + '/', {token: this.getters.orderData.token, payload: newOrder.map((order) => order.id)}).then((response) => {
          commit('setOrdered', newOrder)
          commit('setOrderData', response.data)
        })
      }
      commit('setOrdered', newOrder)
    },
    checkoutOrder ({ commit }) {
      api.put('/orders/' + this.getters.orderData.number + '/',
        {
          token: this.getters.orderData.token
        }).then((response) => {
        commit('setOrdered', [])
        commit('setOrderData', {})
        alert(response.data.number)
      })
    }
  }
})
