// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './components/App'
import router from './router'
import store from './store'
import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import VueLocalStorage from 'vue-localstorage'

Vue.use(VueLocalStorage)
Vue.use(VueMaterial)

Vue.config.productionTip = false

// eslint-disable-next-line
Array.prototype.recursiveFind = function (name) {
  for (let i = 0; i < this.length; i++) {
    if (this[i].name === name) {
      return this[i]
    } else if (this[i].children.length !== 0) {
      let result = this[i].children.recursiveFind(name)
      if (result) {
        return result
      } else {
        continue
      }
    }
  }
  return null
}

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})
